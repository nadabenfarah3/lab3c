CC=gcc
CFLAGS=-std=c99 -Wall

clean:
    rm -f *.o test *.gcov *.gcda *.gcno
